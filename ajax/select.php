<?xml version="1.0" encoding="UTF-8"?>
<!--
The MIT License

Copyright 2017 Laith Zaki (pseudonym 7Hazard).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->
<?php
include_once '../php/sql.php'; // Include the SQL handlers
// Establish MySQL connection
$conn = mysqlconn("leizaki.com", "questioneer", "questpass", "questioneer");

function listSubjects($conn) {
    $sql = $conn->query("SELECT id, category, subject FROM Subjects");
    
    $curcat = "";
    echo '<div class="hide"><select>';
    while ($row = $sql->fetch_assoc()){
        if ($curcat != $row["category"]){
            echo '</select></div>
            <div class="input field col s4"><label>'.$row["category"].'</label>
                <select name="categories['.$row["category"].'[]]" multiple>
                    <option value="'.$row["category"].'" disabled selected>'
                        .$row["category"].'</option>
                    <option value="'.$row["id"].'">'.$row["subject"].'</option>
            ';
            $curcat = $row["category"];
        } else {
            echo '<option value="'.$row["id"].'">'.$row["subject"].'</option>';
        }
    }
    echo '</select></div>';
}
?>
<!DOCTYPE html>
<div class="card sticky-action center-align light-blue darken-4">
    <form id="subjectsForm" action="ajax/confirm.php" method="POST">
    <div class="card-content white-text">
        <span class="card-title">Pick Categories</span>
        <div class="row">
            <?php
            listSubjects($conn);
            ?>
        </div>
    </div>
    <div class="card-action">
        <a class="btn teal darken-1" href="#" onclick="prevStart()"><- Back</a>
        <button class="btn teal darken-1" type="submit" onclick="nextConfirm()">Next -></button>
    </div>
</form>
</div>
<script>
    document.title = "Questioneer - Selection";
    $('select').material_select();
</script>
<?php
$conn->close(); // Close the Database connection