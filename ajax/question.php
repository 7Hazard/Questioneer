<!DOCTYPE html>
<!--
The MIT License

Copyright 2017 Laith Zaki (pseudonym 7Hazard).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->
<?php
session_start();
// Declare the respective variables used throughout the question module
$qid;
$subjectid;
$question;
$difficulty;
$correct;
$answer1;
$answer2;
$answer3;
$answer4;
    
include_once '../php/sql.php'; // Include the SQL handlers
// Establish MySQL connection
$conn = mysqlconn("leizaki.com", "questioneer", "questpass", "questioneer");

function getSubjects(){ return implode("' OR subjectid='", $_SESSION["categories"]); }
function checkAnswer($answer, $correctans){
    if ($answer == $correctans) return 'correct';
    else return 'incorrect';
}

// Prepare SQL query statement
$sql = $conn->query("SELECT * FROM Questions WHERE subjectid='".getSubjects()."' ORDER BY RAND() LIMIT 1");

// Fetch the row results from the table and process
while($row = $sql->fetch_assoc()){
    // Assign the respective variables used throughout the question module
    $_SESSION["qid"] = $row["id"];
    $_SESSION["subjectid"] = $row["subjectid"];
    $_SESSION["question"] = $row["question"];
    $_SESSION["difficulty"] = $row["difficulty"];
    $_SESSION["answer1"] = $row["answer1"];
    $_SESSION["answer2"] = $row["answer2"];
    $_SESSION["answer3"] = $row["answer3"];
    $_SESSION["answer4"] = $row["answer4"];
    
    $_SESSION["ansid1"] = str_replace(' ', '', $row["answer1"]);
    $_SESSION["ansid2"] = str_replace(' ', '', $row["answer2"]);
    $_SESSION["ansid3"] = str_replace(' ', '', $row["answer3"]);
    $_SESSION["ansid4"] = str_replace(' ', '', $row["answer4"]);
    $_SESSION["correctid"] = str_replace(' ', '', $row[$row["correct"]]);
    
    $_SESSION["ansid1"] = str_replace('#', '-hash', $_SESSION["ansid1"]);
    $_SESSION["ansid2"] = str_replace('#', '-hash', $_SESSION["ansid2"]);
    $_SESSION["ansid3"] = str_replace('#', '-hash', $_SESSION["ansid3"]);
    $_SESSION["ansid4"] = str_replace('#', '-hash', $_SESSION["ansid4"]);
    $_SESSION["correctid"] = str_replace('#', '-hash', $_SESSION["correctid"]);
}
?>
<!DOCTYPE html>
<div class="card sticky-action center-align light-blue darken-4">
    <div id="questcontent" class="card-content white-text row">
        <p><?php echo $_SESSION["subjectid"]; ?></p>
        <span class="card-title"><?php echo $_SESSION["question"]; ?></span>
        <form class="container" id="questAnswer" action="ajax/confirm.php" method="POST">
            <?php
            echo '<a class="btn col s12 ans" id="'.$_SESSION["ansid1"].'" onclick="showResult('."'".$_SESSION["ansid1"]."'".')">'.$_SESSION["answer1"].'</a>';
            echo '<a class="btn col s12 ans" id="'.$_SESSION["ansid2"].'" onclick="showResult('."'".$_SESSION["ansid2"]."'".')">'.$_SESSION["answer2"].'</a>';
            if ($_SESSION["answer3"] != 'NULL')
                echo '<a class="btn col s12 ans" id="'.$_SESSION["ansid3"].'" onclick="showResult('."'".$_SESSION["ansid3"]."'".')">'.$_SESSION["answer3"].'</a>';
            if ($_SESSION["answer4"] != 'NULL')
                echo '<a class="btn col s12 ans" id="'.$_SESSION["ansid4"].'" onclick="showResult('."'".$_SESSION["ansid4"]."'".')">'.$_SESSION["answer4"].'</a>';
            ?>            
        </form>
    </div>
    <div class="card-action" style="a{margin: 0 2%;}">
        <a class="btn teal darken-1" href="#" onclick="prevSelect()"><- Reselect</a>
        <a class="btn teal darken-1" href="#" id="nquest" onclick="nextQuestions()">Skip -></a>
    </div>
</div>
<script src="js/questions.js"></script>
<script>
    document.title = "Questioneer - Playing!";
    setAnswered(false);
    setCorrect("<?php echo $_SESSION["correctid"]; ?>"); // Assign the correct answer for JS scripting
</script>