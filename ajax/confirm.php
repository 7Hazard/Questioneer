<!DOCTYPE html>
<!--
The MIT License

Copyright 2017 Laith Zaki (pseudonym 7Hazard).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->
<?php
session_start();
?>
<!DOCTYPE html>
<div class="card sticky-action center-align light-blue darken-4">
    <div class="card-content white-text row">
        <span class="card-title">Confirm Selection</span>
        <div class="col s12">
        <?php
        // Check wether the POST form from earlier is properly submitted
        if(!empty($_POST["categories"])){
            // Make a chip for each subject select from category
            foreach ($_POST['categories'] as $option)
                echo '<div class="chip">'.$option.'</div>';
            $_SESSION["categories"] = $_POST["categories"];
        } else echo '<h5>You have not selected any subjects!</h2>
                    <p>Return and select subjects to continue...</p>';
        ?>
        </div>
    </div>
    <div class="card-action">
        <a class="btn teal darken-1" href="#" onclick="prevSelect()"><- Back</a>
        <?php if(!empty($_POST["categories"])) echo '<a class="btn teal darken-1" href="#" onclick="nextQuestions()">Confirm -></a>'; ?>
    </div>
</div>
<div id="scripts">
    
</div>
<script>
    document.title = "Questioneer - Confirm Selection";
</script>