<?php

/* 
 * The MIT License
 *
 * Copyright 2017 Laith Zaki (pseudonym 7Hazard).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* MySQL connector (with adress, username, password and dbname as params)
 * returns connection object
 */
function mysqlconn($serveradress, $username, $password, $dbname){
    // Define local mysqli connection variable and connect
    $conn = new mysqli($serveradress, $username, $password, $dbname);
    // Check wether the mysqli connection has an error
    if ($conn->connect_error) {
        // if not successful, kill the connection and output the error
        die("Connection failed: " . $conn->connect_error);
    } else {
        // if successful, return the connection
        return $conn;
    }
}

/* 
 * Database Category table checker
 */
function checkCategories($conn) {
    //Prepare Table check query
    $result = $conn->query("SHOW TABLES LIKE 'Categories'");
    // Check wether there exists any rows in the Categories table
    if($result->num_rows == 1) {
        echo "Table Categories exists <br>";
    }
    else {
        echo "Table Categories does not exist, creating table! <br>";

        $table = "CREATE TABLE Categories (
            name VARCHAR(30) NOT NULL PRIMARY KEY UNIQUE
            );";

        if ($conn->query($table) === TRUE) {
            echo "Successfully constructed Categories table! <br>";
        } else {
            echo "Error creating Categories table: " . $conn->error . "<br>";
        }
    }
    echo "<br>";
}

/* 
 * Database Subject table checker
 */
function checkSubjects($conn) {
    // Query, read comment below
    $result = $conn->query("SHOW TABLES LIKE 'Subjects'");
    // Check wether there exists any rows in the Subjects table
    if($result->num_rows == 1) {
        echo "Table Subjects exists <br>";
    }
    else {
        echo "Table Subjects does not exist, creating table! <br>";
        
        $table = "CREATE TABLE Subjects (
            id VARCHAR(30) NOT NULL PRIMARY KEY UNIQUE,
            category VARCHAR(30) NOT NULL,
            subject VARCHAR(30) NOT NULL
            );";
        // Query the row to the database, return bool wether it was successful
        if ($conn->query($table) === TRUE) {
            echo "Successfully constructed Subjects table!<br>";
        } else {
            echo "Error creating Subjects table: " . $conn->error . "<br>";
        }
    }
    echo "<br>";
}

/*
 * Database Questions table checker
 */
function checkQuestions($conn){
    // Query, read comment below
    $result = $conn->query("SHOW TABLES LIKE 'Questions'");

    // Check wether there exists any rows in the Questions table
    if($result->num_rows == 1) {
        echo "Table Questions exists <br>";
    }
    else {
        echo "Table Questions does not exist, creating table! <br>";

        $table = "CREATE TABLE Questions (
            id int NOT NULL PRIMARY KEY UNIQUE AUTO_INCREMENT,
            subjectid VARCHAR(30) NOT NULL,
            question VARCHAR(100) NOT NULL UNIQUE,
            difficulty VARCHAR(10) NOT NULL,
            correct VARCHAR(10) NOT NULL,
            answer1 VARCHAR(30) NOT NULL,
            answer2 VARCHAR(30) NOT NULL,
            answer3 VARCHAR(30),
            answer4 VARCHAR(30)
            );";
        // Query the row to the database, return bool wether it was successful
        if ($conn->query($table) === TRUE) {
            echo "Successfully constructed Questions table!<br>";
        } else {
            echo "Error creating Questions table: " . $conn->error . "<br>";
        }
    }
    echo "<br>";
}

/*
 * Database Category adder
 */
function addCategory($conn, $category) {
    // Prepare Table check query
    $row = "INSERT INTO Categories (category)
            VALUES ('".$category."')";
    // Query the row to the database, return bool wether it was successful
    if ($conn->query($row) === TRUE) {
        echo "Successfully added category row!<br>";
    } else {
        echo "Error adding category row: " . $conn->error . "<br>";
    }
    echo "<br>";
}

/*
 * Database Subject adder
 */
function addSubject($conn, $subject, $category) {
    // Prepare Table check query
    $row = "INSERT INTO Subjects (id, category, subject)
            VALUES ('".$category."/".$subject."', '".$category."', '".$subject."')";
    // Query the row to the database, return bool wether it was successful
    if ($conn->query($row) === TRUE) {
        echo "Successfully added subject row!<br>";
    } else {
        echo "Error adding subject row: " . $conn->error . "<br>";
    }
    echo "<br>";
}

/*
 * Database Question adder
 * Having a third or a fourth answer is optional
 */
function addQuestion($conn, $subjectid, $question, $difficulty, $correct,
                    $answer1, $answer2, $answer3 = 'NULL', $answer4 = 'NULL') {
    // Check wether the correct answer is the same as any of the available options
    if($correct == 'answer1' || $correct == 'answer2' || $correct == 'answer3' || $correct == 'answer4'){
        // Prepare Table check query
        $row = "INSERT INTO Questions (subjectid, question, difficulty, correct, answer1, answer2, answer3, answer4)
                VALUES ('".$subjectid."', '".$question."', '".$difficulty."', '".$correct."',
                '".$answer1."', '".$answer2."', '".$answer3."', '".$answer4."')";
        // Query the row to the database, return bool wether it was successful
        if ($conn->query($row) === TRUE) {
            echo "Successfully added Question row!<br>";
        } else {
            echo "Error adding Question row: " . $conn->error . "<br>";
        }        
    } else echo "Error adding Question row:
                Correct answer does not equal to any of the answer options!<br>";
    echo "<br>";
}