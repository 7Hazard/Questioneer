<!DOCTYPE html>
<!--
The MIT License

Copyright 2017 Laith Zaki (pseudonym 7Hazard).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /* 
        * Database table checker
        */
        
        include_once 'sql.php'; // Include SQL function ONCE
        
        //Establish MySQL Connection
        $conn = mysqlconn("leizaki.com", "questioneer", "questpass", "questioneer");
        checkCategories($conn);
        checkSubjects($conn);
        checkQuestions($conn);
        
        addCategory($conn, "Games");
        addSubject($conn, "History", "Games");
        addSubject($conn, "WWII", "History");
        addSubject($conn, "Computers", "Technology");

        addQuestion($conn, 'Technology/Programming', 'Which language does C++ derive from?', 'Easy', 'answer3', 'C#', 'HTML', 'C', 'Java');
        addQuestion($conn, 'Technology/Programming', 'Can you use parameters in functions?', 'Medium', 'answer1', 'Yes', 'No');
        addQuestion($conn, 'Technology/Programming', 'Where is the language JavaScript mostly used?', 'Easy', 'answer2', 'Industry Machines', 'Internet', 'Graphics', 'Quantum Physics');
        addQuestion($conn, 'Games/History', 'Who were the developers of Half-Life?', 'Easy', 'answer2', 'DICE', 'Valve', 'EA Sports', 'Ubisoft');
        addQuestion($conn, 'History/WWII', 'What year did Adolf Hitler die?', 'Easy', 'answer4', '1939', '2001', '1894', '1945');
        addQuestion($conn, 'History/WWII', 'When did Adolf Hitler take full power of Germany?', 'Medium', 'answer1', '1933', '1999', '1945');
        addQuestion($conn, 'Technology/Computers', 'What is a CPU?', 'Easy', 'answer4', 'Screen', 'Graphics Card', 'Mouse', 'Processor');
        
        $conn->close();
        ?>
    </body>
</html>
