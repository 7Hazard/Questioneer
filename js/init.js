/* 
 * The MIT License
 *
 * Copyright 2017 Laith Zaki (pseudonym 7Hazard).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* Page AJAX'ers */
function nextStart(){
    $.ajax({url: "ajax/start.php", success: function(result){
        $("#maincontent").html(result);
        $('#maincontent').addClass('fadeIn');
        $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function(){
                $('#maincontent').removeClass('fadeIn');
            });
    }});
}

function prevStart(){
    $.ajax({url: "ajax/start.php", success: function(result){
        $('#maincontent').addClass('fadeOutRight');
        $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function(){
                $("#maincontent").html(result);
                $('#maincontent').removeClass('fadeOutRight');
                $('#maincontent').addClass('fadeInLeft');
                $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                    function(){
                        $('#maincontent').removeClass('fadeInLeft');
                    });
            });
    }});
}

function nextSelect(){
    $.ajax({url: "ajax/select.php", success: function(result){
        $('#maincontent').addClass('fadeOutLeft');
        $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function(){
                $("#maincontent").html(result);
                $('#maincontent').removeClass('fadeOutLeft');
                $('#maincontent').addClass('fadeInRight');
                $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                    function(){
                        $('#maincontent').removeClass('fadeInRight');
                    });
            });
    }});
}

function prevSelect(){
    $.ajax({url: "ajax/select.php", success: function(result){
        $('#maincontent').addClass('fadeOutRight');
        $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function(){
                $("#maincontent").html(result);
                $('#maincontent').removeClass('fadeOutRight');
                $('#maincontent').addClass('fadeInLeft');
                $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                    function(){
                        $('#maincontent').removeClass('fadeInLeft');
                    });
            });
    }});
}

function nextConfirm(){
    $("#subjectsForm").submit(function(e) {
        $.ajax({
            type: "POST", url: "ajax/confirm.php",
            data: $("#subjectsForm").serialize(), // serializes the form's elements
            success: function(result){
            $('#maincontent').addClass('fadeOutLeft');
            $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                function(){
                    $("#maincontent").html(result);
                    $('#maincontent').removeClass('fadeOutLeft');
                    $('#maincontent').addClass('fadeInRight');
                    $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                        function(){
                            $('#maincontent').removeClass('fadeInRight');
                        });
                });
        }});
        e.preventDefault(); // avoid to execute the actual submit of the form
    });
}

function prevConfirm(){
    $.ajax({url: "ajax/confirm.php", success: function(result){
        $('#maincontent').addClass('fadeOutRight');
        $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function(){
                $("#maincontent").html(result);
                $('#maincontent').removeClass('fadeOutRight');
                $('#maincontent').addClass('fadeInLeft');
                $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                    function(){
                        $('#maincontent').removeClass('fadeInLeft');
                    });
            });
    }});
}

function nextQuestions(){
    $.ajax({url: "ajax/question.php", success: function(result){
        $('#maincontent').addClass('fadeOutLeft');
        $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function(){
                $("#maincontent").html(result);
                $('#maincontent').removeClass('fadeOutLeft');
                $('#maincontent').addClass('fadeInRight');
                $('#maincontent').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
                    function(){
                        $('#maincontent').removeClass('fadeInRight');
                    });
            });
    }});
}